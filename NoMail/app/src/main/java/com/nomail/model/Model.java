package com.nomail.model;

import android.app.Application;
import android.content.Context;

import com.nomail.model.api.API;
import com.nomail.model.api.IResultatArray;
import com.nomail.model.api.IResultatObjet;
import com.nomail.model.api.WebAPI;

public class Model {
    private static Model sInstance = null;
    private final API mApi;

    public static Model getInstance(Application application, Context context){
        if (sInstance == null) {
            sInstance = new Model(application, context);
        }
        return sInstance;
    }

    private Model(Application application, Context context) {
        mApi = new WebAPI(application, context);
    }

    public void login(String email, String password, String pdp, IResultatObjet callback){
        mApi.login(email, password, pdp, callback);
    }

    public void register(String email, String password, String pdp, IResultatObjet callback){
        mApi.register(email, password, pdp, callback);
    }

    public void validerToken(String token, IResultatObjet callback){
        mApi.validerToken(token, callback);
    }

	public void getCourrielsRecus(String token, IResultatArray callback) {
        mApi.getCourrielsRecus(token, callback);
	}

	public void getCourrielsEnvoyes(String token, IResultatArray callback) {
        mApi.getCourrielsEnvoyes(token, callback);
    }

    public void getCourrielsArchives(String token, IResultatArray callback) {
        mApi.getCourrielsArchives(token, callback);
    }

    public void postCourriel(String token,String destinataire, String objet, String message,IResultatObjet callback) {
        mApi.postCourriel(token, destinataire, objet, message, callback);
    }

    public void getCourrielRecu(String token, String id, IResultatObjet callback) {
        mApi.getCourrielRecu(token, id, callback);
    }

    public void getCourrielEnvoye(String token, String id, IResultatObjet callback) {
        mApi.getCourrielEnvoye(token, id, callback);
    }

    public void getCourrielArchive(String token, String id, IResultatObjet callback) {
        mApi.getCourrielArchive(token, id, callback);
    }

    public void patchCourrielArchive(String token, String id, IResultatObjet callback) {
        mApi.patchCourrielArchive(token, id, callback);
    }

    public void deleteCourrielRecu(String token, String id, IResultatObjet callback) {
        mApi.deleteCourrielRecu(token, id, callback);
    }

    public void deleteCourrielEnvoye(String token, String id, IResultatObjet callback) {
        mApi.deleteCourrielEnvoye(token, id, callback);
    }
}
