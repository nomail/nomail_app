package com.nomail.model.api;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface IResultatArray {
	public void surSucces(String requestType, JSONArray response);
	public void surErreur(String requestType, VolleyError error);
}
