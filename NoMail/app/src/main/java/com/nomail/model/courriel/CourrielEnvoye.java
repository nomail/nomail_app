package com.nomail.model.courriel;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@RequiresApi(api = Build.VERSION_CODES.O)
public class CourrielEnvoye {

	private Boolean estSelectionne;

	private String id, objet, expediteur, destinataire, dateEnvoi, contenu;
	private Boolean lu, supprime;

	public CourrielEnvoye(String id, String lu, String supprime, String objet, String expediteur, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.supprime=Boolean.parseBoolean(supprime);
		this.objet=objet;
		this.expediteur=expediteur;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("America/Montreal")).format(formatter):"";
	}

	public CourrielEnvoye(String id, String lu, String objet, String destinataire, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.objet=objet;
		this.destinataire=destinataire;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("America/Montreal")).format(formatter):"";
	}

	public CourrielEnvoye(String id, String lu, String objet, String contenu, String expediteur, String destinataire, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.objet=objet;
		this.contenu=contenu;
		this.expediteur=expediteur;
		this.destinataire=destinataire;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("America/Montreal")).format(formatter):"";
	}

	public CourrielEnvoye(String id, String lu, String supprime, String objet, String contenu, String expediteur, String destinataire, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.supprime=Boolean.parseBoolean(supprime);
		this.objet=objet;
		this.contenu=contenu;
		this.expediteur=expediteur;
		this.destinataire=destinataire;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("America/Montreal")).format(formatter):"";
	}

	public CourrielEnvoye(String id, Boolean lu, Boolean supprime, String objet, String expediteur, LocalDateTime dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=lu;
		this.supprime=supprime;
		this.objet=objet;
		this.expediteur=expediteur;
		this.dateEnvoi= !dateEnvoi.equals("")?dateEnvoi.format(formatter):"";
	}

	public void setEstSelectionne(boolean selectionne){
		this.estSelectionne=selectionne;
	}

	public boolean estSelectionne(){
		return estSelectionne;
	}

	public String getId() {
		return id;
	}

	public Boolean getLu() {
		return lu;
	}

	public Boolean getSupprime() {
		return supprime;
	}

	public String getObjet() {
		return objet;
	}

	public String getContenu() {
		return contenu;
	}

	public String getExpediteur() {
		return expediteur;
	}

	public String getDestinataire() {
		return destinataire;
	}

	public String getDateEnvoi() {
		return dateEnvoi;
	}

	@Override
	public String toString() {
		return "CourrielRecu{" +
				"id='" + id + '\'' +
				", objet='" + objet + '\'' +
				", expediteur='" + expediteur + '\'' +
				", lu=" + lu +
				", supprimé=" + supprime +
				", dateEnvoi=" + dateEnvoi +
				'}';
	}
}
