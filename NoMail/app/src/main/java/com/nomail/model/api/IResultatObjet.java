package com.nomail.model.api;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface IResultatObjet {
	public void surSucces(String requestType, JSONObject response);
	public void surErreur(String requestType, VolleyError error);
}
