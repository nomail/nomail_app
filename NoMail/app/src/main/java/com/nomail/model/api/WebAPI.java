package com.nomail.model.api;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.nomail.BuildConfig;
import com.nomail.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


public class WebAPI implements API {
	final String TAG = "WebAPI";
	private final RequestQueue requestQueue;
	IResultatObjet mCallbackResultat;
	IResultatArray mCallbackResultatArray;
	Context mContext;

	public WebAPI(Application app, Context context) {
		requestQueue = Volley.newRequestQueue(app);
		mContext=context;
	}

	@Override
	public void login(String courriel, String motDePasse, String phraseDePasse, IResultatObjet callback) {
		mCallbackResultat = callback;

		String url = BuildConfig.SERVEUR_API + "connexion";

		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);
		SharedPreferences.Editor editor = sharedpreferences.edit();

		try {
			Map<String, String> parametres = new HashMap<String, String>();
			parametres.put("adresseCourriel", courriel);
			parametres.put("motDePasse", motDePasse);
			parametres.put("phraseDePasse", phraseDePasse);

			editor.putString("adresseCourriel", courriel);
			editor.apply();

			JsonObjectRequest jsonObj = new JsonObjectRequest(url, new JSONObject(parametres), new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					if(mCallbackResultat != null){
						mCallbackResultat.surSucces("POSTCALL",response);
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if(mCallbackResultat != null){
						mCallbackResultat.surErreur("POSTCALL",error);
					}
				}
			});

			jsonObj.setShouldCache(false);
			jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
			requestQueue.add(jsonObj);

		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void register(String courriel, String motDePasse, String phraseDePasse, IResultatObjet callback) {
		mCallbackResultat = callback;

		String url = BuildConfig.SERVEUR_API + "inscription";

		try {
			Map<String, String> parametres = new HashMap<String, String>();
			parametres.put("adresseCourriel", courriel);
			parametres.put("motDePasse", motDePasse);
			parametres.put("phraseDePasse", phraseDePasse);

			JsonObjectRequest jsonObj = new JsonObjectRequest(url, new JSONObject(parametres), new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					if(mCallbackResultat != null)
						Log.d(TAG, "RÉPONSE" + response);
						mCallbackResultat.surSucces("POSTCALL",response);
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if(mCallbackResultat != null)
						Log.d(TAG, "ERREUR" + error);
					mCallbackResultat.surErreur("POSTCALL",error);
				}
			});

			requestQueue.add(jsonObj);

		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void validerToken(String token, IResultatObjet callback) {
		mCallbackResultat = callback;

		String url = BuildConfig.SERVEUR_API + "token";

		try {
			JsonObjectRequest jsonObj = new JsonObjectRequest(url, new JSONObject(new HashMap<String, String>()), new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					if(mCallbackResultat != null)
						mCallbackResultat.surSucces("POSTCALL",response);
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if(mCallbackResultat != null)
						mCallbackResultat.surErreur("POSTCALL",error);
				}
			}) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers = new HashMap<String, String>();
					headers.put("Authorization", token);
					return headers;
				}
			};

			jsonObj.setShouldCache(false);
			jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
			requestQueue.add(jsonObj);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getCourrielsRecus(String token, IResultatArray callback) {
		mCallbackResultatArray = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);
		byte[] adresseCourrielUtilisateur = sharedpreferences.getString("adresseCourriel", "").getBytes(StandardCharsets.UTF_8);
		String adresseCourrielUtilisateurBase64 = Base64.encodeToString(adresseCourrielUtilisateur, Base64.DEFAULT).replace("=", "");

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/recu/"+adresseCourrielUtilisateurBase64;

			try {
				JsonArrayRequest jsonObj = new JsonArrayRequest(Request.Method.GET, url, new JSONArray(), new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {
						if (mCallbackResultatArray != null)
							mCallbackResultatArray.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultatArray != null)
							mCallbackResultatArray.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void getCourrielsEnvoyes(String token, IResultatArray callback) {
		mCallbackResultatArray = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);
		byte[] adresseCourrielUtilisateur = sharedpreferences.getString("adresseCourriel", "").getBytes(StandardCharsets.UTF_8);
		String adresseCourrielUtilisateurBase64 = Base64.encodeToString(adresseCourrielUtilisateur, Base64.DEFAULT).replace("=", "");

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/envoye/"+adresseCourrielUtilisateurBase64;

			try {
				JsonArrayRequest jsonObj = new JsonArrayRequest(Request.Method.GET, url, new JSONArray(), new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {
						if (mCallbackResultatArray != null)
							mCallbackResultatArray.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultatArray != null)
							mCallbackResultatArray.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void getCourrielsArchives(String token, IResultatArray callback) {
		mCallbackResultatArray = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);
		byte[] adresseCourrielUtilisateur = sharedpreferences.getString("adresseCourriel", "").getBytes(StandardCharsets.UTF_8);
		String adresseCourrielUtilisateurBase64 = Base64.encodeToString(adresseCourrielUtilisateur, Base64.DEFAULT).replace("=", "");

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/recu/archive/"+adresseCourrielUtilisateurBase64;

			try {
				JsonArrayRequest jsonObj = new JsonArrayRequest(Request.Method.GET, url, new JSONArray(), new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {
						if (mCallbackResultatArray != null)
							mCallbackResultatArray.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultatArray != null)
							mCallbackResultatArray.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void postCourriel(String token,String destinataire, String objet, String message, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url = BuildConfig.SERVEUR_API + "courriels";
		byte[] adresseDestinataire = destinataire.getBytes(StandardCharsets.UTF_8);
		String adresseDestinataireBase64 = Base64.encodeToString(adresseDestinataire, Base64.DEFAULT).replace("=", "");

		try {
			Map<String, String> parametres = new HashMap<String, String>();
			parametres.put("objet", objet);
			parametres.put("contenu", message);
			parametres.put("adresseDestinataire", adresseDestinataireBase64.trim());
			Log.d("WEBAPI postCourriel","Destinataire base 64 -"+adresseDestinataireBase64+"-");

			JsonObjectRequest jsonObj = new JsonObjectRequest(url, new JSONObject(parametres), new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					if (mCallbackResultat != null) {
						mCallbackResultat.surSucces("POSTCALL", response);
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if (mCallbackResultat != null) {
						mCallbackResultat.surErreur("POSTCALL", error);
					}
				}
			})
			{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Authorization", token);
				return headers;
				}
			};

			jsonObj.setShouldCache(false);
			jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
			requestQueue.add(jsonObj);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getCourrielRecu(String token, String id, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/recu/"+id;

			try {
				JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						if (mCallbackResultat != null)
							mCallbackResultat.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultat != null)
							mCallbackResultat.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void getCourrielEnvoye(String token, String id, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/envoye/"+id;

			try {
				JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						if (mCallbackResultat != null)
							mCallbackResultat.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultat != null)
							mCallbackResultat.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void getCourrielArchive(String token, String id, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/recu/archive/"+id;

			try {
				JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						if (mCallbackResultat != null)
							mCallbackResultat.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultat != null)
							mCallbackResultat.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void patchCourrielArchive(String token, String id, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/recu/archive/"+id;

			try {
				JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(), new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						if (mCallbackResultat != null)
							mCallbackResultat.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultat != null)
							mCallbackResultat.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void deleteCourrielRecu(String token, String id, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/recu/"+id;

			try {
				JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						if (mCallbackResultat != null)
							mCallbackResultat.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultat != null)
							mCallbackResultat.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void deleteCourrielEnvoye(String token, String id, IResultatObjet callback) {
		mCallbackResultat = callback;
		String url;
		SharedPreferences sharedpreferences = mContext.getSharedPreferences("NoMailCacheClient", 0);

		if (!sharedpreferences.getString("adresseCourriel", "").equals("")) {
			url = BuildConfig.SERVEUR_API + "courriels/envoye/"+id;

			try {
				JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						if (mCallbackResultat != null)
							mCallbackResultat.surSucces("GETCALL", response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if (mCallbackResultat != null)
							mCallbackResultat.surErreur("GETCALL", error);
					}
				}) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers = new HashMap<String, String>();
						headers.put("Authorization", token);
						return headers;
					}
				};
				jsonObj.setShouldCache(false);
				jsonObj.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 1.0f));
				requestQueue.add(jsonObj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
		}
	}
}
