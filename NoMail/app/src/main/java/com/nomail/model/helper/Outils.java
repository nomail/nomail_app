package com.nomail.model.helper;

public class Outils {
	public static boolean isNaN(String s) {
		if(s.isEmpty()) return true;
		for(int i = 0; i < s.length(); i++) {
			if(i == 0 && s.charAt(i) == '-') {
				if(s.length() == 1) return true;
				else continue;
			}
			if(Character.digit(s.charAt(i),10) < 0) return true;
		}
		return false;
	}
}

