package com.nomail.activite.ui.voirCourriel;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.nomail.R;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatObjet;
import com.nomail.model.courriel.CourrielEnvoye;
import com.nomail.model.courriel.CourrielRecu;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;


public class FragmentVoirCourriel extends Fragment {

	final String TAG = "FragmentVoirCourriel";

	private ProgressBar barreChargement;
	Bundle bundle;
	TextView objet, expediteur, dateEnvoi, contenu, destinataire;
	String courrielRecuTmp, courrielEnvoyeTmp, courrielArchiveTmp;

	ConstraintLayout layoutCourriel;

	CourrielRecu courrielRecu;
	CourrielEnvoye courrielEnvoye;
	IResultatObjet mResultatCallbackObjetRecu, mResultatCallbackObjetEnvoye, mResultatCallbackObjetArchive;
	Model model;

	@RequiresApi(api = Build.VERSION_CODES.O)
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.bundle = this.getArguments();

		View view = inflater.inflate(R.layout.fragment_voir_courriel, container, false);
		layoutCourriel = view.findViewById(R.id.constraint_layout_voir_courriel);

		showLoadingSpinner(view);

		SharedPreferences sharedpreferences = requireContext().getSharedPreferences("NoMailCacheClient", 0);
		String token = sharedpreferences.getString("token", "");

		model = Model.getInstance(requireActivity().getApplication(), getContext());

		courrielRecuTmp =  this.bundle.getString("idCourrielRecu");
		courrielEnvoyeTmp =  this.bundle.getString("idCourrielEnvoye");
		courrielArchiveTmp =  this.bundle.getString("idCourrielArchive");

		if (courrielRecuTmp!=null) {
			initVolleyCallbackCourrielRecu();
			model.getCourrielRecu(token, courrielRecuTmp, mResultatCallbackObjetRecu);
		} else if (courrielEnvoyeTmp!=null) {
			initVolleyCallbackCourrielEnvoye();
			model.getCourrielEnvoye(token, courrielEnvoyeTmp, mResultatCallbackObjetEnvoye);
		} else if (courrielArchiveTmp!=null) {
			initVolleyCallbackCourrielArchive();
			model.getCourrielArchive(token, courrielArchiveTmp, mResultatCallbackObjetArchive);
		}
		return view;
	}

	void initVolleyCallbackCourrielRecu() {
		Log.d(TAG, "initVolleyCallbackCourrielRecu");
		mResultatCallbackObjetRecu = new IResultatObjet() {
			@SuppressLint("SetTextI18n")
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
				removeLoadingSpinner();

				try {
					JSONObject objetCourriel = response.getJSONObject("courriel");
					courrielRecu = new CourrielRecu(
							response.getString("_id"),
							response.getString("lu"),
							response.getString("archivé"),
							objetCourriel.getString("objet"),
							objetCourriel.getString("contenu"),
							new String(Base64.decode(objetCourriel.getString("adresseExpéditeur"), Base64.DEFAULT), StandardCharsets.UTF_8),
							new String(Base64.decode(objetCourriel.getString("adresseDestinataire"), Base64.DEFAULT), StandardCharsets.UTF_8),
							response.getString("dateCréation")
					);

					initLabels();
					objet.setText(courrielRecu.getObjet());
					contenu.setText(courrielRecu.getContenu());
					expediteur.setText(getString(R.string.lblCourrielDe) + courrielRecu.getExpediteur());
					destinataire.setText(getString(R.string.lblCourrielA) + courrielRecu.getDestinataire());
					dateEnvoi.setText(courrielRecu.getDateEnvoi());

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				error.printStackTrace();
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);
				removeLoadingSpinner();

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	void initVolleyCallbackCourrielEnvoye() {
		Log.d(TAG, "initVolleyCallbackCourrielEnvoye");
		mResultatCallbackObjetEnvoye = new IResultatObjet() {
			@SuppressLint("SetTextI18n")
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
				removeLoadingSpinner();

				try {
					JSONObject objetCourriel = response.getJSONObject("courriel");
					courrielEnvoye = new CourrielEnvoye(
							response.getString("_id"),
							response.getString("lu"),
							objetCourriel.getString("objet"),
							objetCourriel.getString("contenu"),
							new String(Base64.decode(objetCourriel.getString("adresseExpéditeur"), Base64.DEFAULT), StandardCharsets.UTF_8),
							new String(Base64.decode(objetCourriel.getString("adresseDestinataire"), Base64.DEFAULT), StandardCharsets.UTF_8),
							response.getString("dateCréation")
					);

					initLabels();
					objet.setText(courrielEnvoye.getObjet());
					contenu.setText(courrielEnvoye.getContenu());
					expediteur.setText(getString(R.string.lblCourrielDe) + courrielEnvoye.getExpediteur());
					destinataire.setText(getString(R.string.lblCourrielA) + courrielEnvoye.getDestinataire());
					dateEnvoi.setText(courrielEnvoye.getDateEnvoi());

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);
				removeLoadingSpinner();

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	void initVolleyCallbackCourrielArchive() {
		Log.d(TAG, "initVolleyCallbackCourrielArchive");
		mResultatCallbackObjetArchive = new IResultatObjet() {
			@SuppressLint("SetTextI18n")
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
				removeLoadingSpinner();

				try {
					JSONObject objetCourriel = response.getJSONObject("courriel");
					courrielRecu = new CourrielRecu(
							response.getString("_id"),
							response.getString("lu"),
							response.getString("archivé"),
							objetCourriel.getString("objet"),
							objetCourriel.getString("contenu"),
							new String(Base64.decode(objetCourriel.getString("adresseExpéditeur"), Base64.DEFAULT), StandardCharsets.UTF_8),
							new String(Base64.decode(objetCourriel.getString("adresseDestinataire"), Base64.DEFAULT), StandardCharsets.UTF_8),
							response.getString("dateCréation")
					);

					initLabels();
					objet.setText(courrielRecu.getObjet());
					contenu.setText(courrielRecu.getContenu());
					expediteur.setText(getString(R.string.lblCourrielDe) + courrielRecu.getExpediteur());
					destinataire.setText(getString(R.string.lblCourrielA) + courrielRecu.getDestinataire());
					dateEnvoi.setText(courrielRecu.getDateEnvoi());

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);
				removeLoadingSpinner();

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	private void initLabels(){
		objet = layoutCourriel.findViewById(R.id.lblVoirCourrielObjet);
		contenu = layoutCourriel.findViewById(R.id.lblVoirCourrielContenu);
		expediteur = layoutCourriel.findViewById(R.id.lblVoirCourrielExpediteur);
		destinataire = layoutCourriel.findViewById(R.id.lblVoirCourrielDestinataire);
		dateEnvoi = layoutCourriel.findViewById(R.id.lblVoirCourrielDateEnvoi);
	}

	public void removeLoadingSpinner() {
		try {
			if (barreChargement != null) {
				if (barreChargement.getVisibility()==View.VISIBLE) {
					barreChargement.setVisibility(View.GONE);
					barreChargement = null;
				}
			}
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}

	public void showLoadingSpinner(View view){
		try{
			if (barreChargement==null) {
				barreChargement = view.findViewById(R.id.progressBar_cyclic);
				barreChargement.setVisibility(View.VISIBLE);
				barreChargement.bringToFront();
			}

			if (!(barreChargement.getVisibility()==View.VISIBLE)) {
				barreChargement.setVisibility(View.VISIBLE);
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
