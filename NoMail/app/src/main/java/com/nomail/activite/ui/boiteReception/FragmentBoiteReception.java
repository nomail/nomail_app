package com.nomail.activite.ui.boiteReception;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.google.android.material.snackbar.Snackbar;
import com.nomail.BuildConfig;
import com.nomail.R;
import com.nomail.activite.NavDrawer;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatArray;
import com.nomail.model.courriel.CourrielRecu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;


public class FragmentBoiteReception extends Fragment {

	final String TAG = "FragmentBoiteReception";
	private long timer;

	private ProgressBar barreChargement;
	CourrielRecuAdapter courrielRecuAdapteur;
	RecyclerView recyclerView;

	ArrayList<CourrielRecu> courrielRecuArrayList;
	IResultatArray mResultatCallbackArray;
	Model model;
	Context mContext;

	@RequiresApi(api = Build.VERSION_CODES.O)
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		courrielRecuAdapteur = new CourrielRecuAdapter(getActivity(), requireActivity().getApplication(), courrielRecuArrayList);
		View view = inflater.inflate(R.layout.fragment_boite_reception, container, false);

		recyclerView = view.findViewById(R.id.listeRVCourrielsRecus);

		showLoadingSpinner(view);

		initVolleyCallbackBoiteReception();
		SharedPreferences sharedpreferences = requireContext().getSharedPreferences("NoMailCacheClient", 0);
		String token = sharedpreferences.getString("token", "");

		model = Model.getInstance(getActivity().getApplication(), getContext());
		model.getCourrielsRecus(token, mResultatCallbackArray);
		OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {

			@Override
			public void handleOnBackPressed() {
				if(timer + 2000> System.currentTimeMillis()){
					Intent setIntent = new Intent(Intent.ACTION_MAIN);
					setIntent.addCategory(Intent.CATEGORY_HOME);
					setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(setIntent);
				}
				else{
					Snackbar.make(view, requireContext().getResources().getString(R.string.toast_appuie_sur_precedent_pour_quitter), Snackbar.LENGTH_LONG)
							.setAction("Action", null).show();
				}
				timer = System.currentTimeMillis();
			}
		};

		requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
		return view;
	}

	void initVolleyCallbackBoiteReception() {
		Log.d(TAG, "initVolleyCallbackBoiteReception");
		mResultatCallbackArray = new IResultatArray() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONArray response) {
				Log.d(TAG, "Volley JSON post" + response);
				removeLoadingSpinner();

				courrielRecuArrayList = new ArrayList<>();
				JSONArray dataArray;
				try {
					dataArray = response;

					for (int i = 0; i < dataArray.length(); i++) {
						JSONObject dataobj = dataArray.getJSONObject(i);
						JSONObject objetCourriel = dataobj.getJSONObject("courriel");
						String objetDuCourriel = objetCourriel.getString("objet").length()>BuildConfig.TAILLE_MAX_OBJET_COURRIEL
								?objetCourriel.getString("objet").substring(0, BuildConfig.TAILLE_MAX_OBJET_COURRIEL)+"..."
								:objetCourriel.getString("objet");
						CourrielRecu courrielRecu = new CourrielRecu(
								dataobj.getString("_id"),
								dataobj.getString("lu"),
								dataobj.getString("archivé"),
								objetDuCourriel,
								new String(Base64.decode(objetCourriel.getString("adresseExpéditeur"), Base64.DEFAULT), StandardCharsets.UTF_8),
								dataobj.getString("dateCréation")
								);

						courrielRecuArrayList.add(courrielRecu);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				setupRecycler();
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);
				removeLoadingSpinner();

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	private void setupRecycler(){
		recyclerView.setHasFixedSize(true);
		LinearLayoutManager llm = new LinearLayoutManager(mContext);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recyclerView.setLayoutManager(llm);

		courrielRecuAdapteur = new CourrielRecuAdapter(getActivity(), getActivity().getApplication(), courrielRecuArrayList);
		recyclerView.setAdapter(courrielRecuAdapteur);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
	}

	public void removeLoadingSpinner() {
		try {
			if (barreChargement != null) {
				if (barreChargement.getVisibility()==View.VISIBLE) {
					barreChargement.setVisibility(View.GONE);
					barreChargement = null;
				}
			}
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}

	public void showLoadingSpinner(View view){
		try{
			if (barreChargement==null) {
				barreChargement = view.findViewById(R.id.progressBar_cyclic);
				barreChargement.setVisibility(View.VISIBLE);
				barreChargement.bringToFront();
			}

			if (!(barreChargement.getVisibility()==View.VISIBLE)) {
				barreChargement.setVisibility(View.VISIBLE);
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
