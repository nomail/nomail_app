package com.nomail.activite.ui.boiteEnvoi;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.nomail.BuildConfig;
import com.nomail.R;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatArray;
import com.nomail.model.courriel.CourrielEnvoye;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FragmentBoiteEnvoi extends Fragment {

	final String TAG = "FragmentBoiteEnvoi";

	private ProgressBar barreChargement;
	CourrielEnvoyeAdapter courrielEnvoyeAdapteur;
	RecyclerView recyclerView;

	ArrayList<CourrielEnvoye> courrielEnvoyeArrayList;
	IResultatArray mResultatCallbackArray;
	Model model;
	Context mContext;

	@RequiresApi(api = Build.VERSION_CODES.O)
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		courrielEnvoyeAdapteur = new CourrielEnvoyeAdapter(getActivity(),requireActivity().getApplication(), courrielEnvoyeArrayList);
		View view = inflater.inflate(R.layout.fragment_boite_envoi, container, false);

		recyclerView = view.findViewById(R.id.listeRVCourrielsArchives);

		showLoadingSpinner(view);

		initVolleyCallbackBoiteEnvoi();
		SharedPreferences sharedpreferences = requireContext().getSharedPreferences("NoMailCacheClient", 0);
		String token = sharedpreferences.getString("token", "");

		model = Model.getInstance(requireActivity().getApplication(), getContext());
		model.getCourrielsEnvoyes(token, mResultatCallbackArray);
		return view;
	}

	void initVolleyCallbackBoiteEnvoi() {
		Log.d(TAG, "initVolleyCallbackBoiteEnvoi");
		mResultatCallbackArray = new IResultatArray() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONArray response) {
				Log.d(TAG, "Volley JSON post" + response);
				removeLoadingSpinner();

				courrielEnvoyeArrayList = new ArrayList<>();
				JSONArray dataArray;
				try {
					dataArray = response;

					for (int i = 0; i < dataArray.length(); i++) {
						JSONObject dataobj = dataArray.getJSONObject(i);
						JSONObject objetCourriel = dataobj.getJSONObject("courriel");

						String objetDuCourriel = objetCourriel.getString("objet").length()>BuildConfig.TAILLE_MAX_OBJET_COURRIEL
								?objetCourriel.getString("objet").substring(0, BuildConfig.TAILLE_MAX_OBJET_COURRIEL)+"..."
								:objetCourriel.getString("objet");
						CourrielEnvoye courrielEnvoye = new CourrielEnvoye(
								dataobj.getString("_id"),
								dataobj.getString("lu"),
								objetDuCourriel,
								new String(Base64.decode(objetCourriel.getString("adresseDestinataire"), Base64.DEFAULT), StandardCharsets.UTF_8),
								dataobj.getString("dateCréation")
						);

						courrielEnvoyeArrayList.add(courrielEnvoye);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				setupRecycler();
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);
				removeLoadingSpinner();

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	private void setupRecycler(){
		recyclerView.setHasFixedSize(true);
		LinearLayoutManager llm = new LinearLayoutManager(mContext);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recyclerView.setLayoutManager(llm);

		courrielEnvoyeAdapteur = new CourrielEnvoyeAdapter(getActivity(),requireActivity().getApplication(), courrielEnvoyeArrayList);
		recyclerView.setAdapter(courrielEnvoyeAdapteur);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
	}

	public void removeLoadingSpinner() {
		try {
			if (barreChargement != null) {
				if (barreChargement.getVisibility()==View.VISIBLE) {
					barreChargement.setVisibility(View.GONE);
					barreChargement = null;
				}
			}
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}

	public void showLoadingSpinner(View view){
		try{
			if (barreChargement==null) {
				barreChargement = view.findViewById(R.id.progressBar_cyclic);
				barreChargement.setVisibility(View.VISIBLE);
				barreChargement.bringToFront();
			}

			if (!(barreChargement.getVisibility()==View.VISIBLE)) {
				barreChargement.setVisibility(View.VISIBLE);
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
