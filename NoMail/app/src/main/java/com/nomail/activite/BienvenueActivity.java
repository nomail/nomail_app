package com.nomail.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;


import com.nomail.R;

public class BienvenueActivity extends AppCompatActivity {
	private Handler mHandler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		getSupportActionBar().hide();
		TextView splash_text = (TextView) findViewById(R.id.splash_text);
		splash_text.setText(R.string.bienvenue);

		mHandler = new Handler(Looper.getMainLooper());
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent nav = new Intent(getApplicationContext(), NavDrawer.class);
				startActivity(nav);
			}
		}, 3000);

	}
	@Override
	public void onBackPressed() {

	}

}